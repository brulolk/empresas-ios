//
//  ViewControllerExtension.swift
//  Empresas
//
//  Created by Bruno Vinicius on 19/12/19.
//  Copyright © 2019 Bruno Vinicius. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftMessages


public extension UIViewController {
    typealias returnAlertFunction = (UIAlertAction) -> Void
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        self.present(alert, animated: true)
    }
    
    /// MyExtensions: Hide Keyboard when touching anywhere
    func hideKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    /// MyExtensions: Show MBProgressHud on currente view
    func showProgress() {
        UIApplication.shared.beginIgnoringInteractionEvents()
        MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    
    /// MyExtensions: Hide MBProgressHud on currente view
    func hideProgress() {
        UIApplication.shared.endIgnoringInteractionEvents()
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func showSuccessMessage(_ title: String?, message: String?, duration: Float) {
        let info = MessageView.viewFromNib(layout: .cardView)
        info.configureTheme(.success)
        info.configureContent(title: title, body: message, iconImage: nil,
                              iconText: nil, buttonImage: nil, buttonTitle: "Fechar",
                              buttonTapHandler: { _ in SwiftMessages.hide() })
        var infoConfig = SwiftMessages.defaultConfig
        infoConfig.duration = .seconds(seconds: TimeInterval(duration))
        infoConfig.interactiveHide = true
        infoConfig.presentationStyle = .bottom
        SwiftMessages.show(config: infoConfig, view: info)
    }
    
    func showInfoMessage(_ title: String?, message: String?, duration: Float) {
        let info = MessageView.viewFromNib(layout: .cardView)
        info.configureTheme(.info)
        info.configureContent(title: title, body: message, iconImage: nil,
                              iconText: nil, buttonImage: nil, buttonTitle: "Fechar",
                              buttonTapHandler: { _ in SwiftMessages.hide() })
        var infoConfig = SwiftMessages.defaultConfig
        infoConfig.duration = .seconds(seconds: TimeInterval(duration))
        infoConfig.interactiveHide = true
        infoConfig.presentationStyle = .bottom
        SwiftMessages.show(config: infoConfig, view: info)
    }
    
    func showErrorMessage(_ title: String?, message: String?, duration: Float) {
        let info = MessageView.viewFromNib(layout: .cardView)
        info.configureTheme(.error)
        info.configureContent(title: title, body: message, iconImage: nil,
                              iconText: nil, buttonImage: nil, buttonTitle: "Fechar",
                              buttonTapHandler: { _ in SwiftMessages.hide() })
        var infoConfig = SwiftMessages.defaultConfig
        infoConfig.duration = .seconds(seconds: TimeInterval(duration))
        infoConfig.interactiveHide = true
        infoConfig.presentationStyle = .bottom
        SwiftMessages.show(config: infoConfig, view: info)
    }
    
    func hideMessage() {
        SwiftMessages.hide()
    }
    
}
