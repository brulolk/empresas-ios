//
//  WebHosts.swift
//  Empresas
//
//  Created by Bruno Vinicius on 19/12/19.
//  Copyright © 2019 Bruno Vinicius. All rights reserved.
//

import Foundation

let URL_BASE = "https://empresas.ioasys.com.br/api/v1/"
let URL_SINGIN = URL_BASE + "users/auth/sign_in"
let URL_SHOW = URL_BASE + "enterprises/"
let URL_EMPRESA = URL_BASE + "enterprises?name="
