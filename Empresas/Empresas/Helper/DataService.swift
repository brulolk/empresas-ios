//
//  DataService.swift
//  Empresas
//
//  Created by Bruno Vinicius on 19/12/19.
//  Copyright © 2019 Bruno Vinicius. All rights reserved.
//


import Foundation
import Alamofire

class DataService{
    
    var token = ""
    var client = ""
    var uid = ""
    
    // Referencia compartilhada
    static let shared: DataService = DataService()
    
    // Private init for singleton
    private init(){}
    
    func createHeaders(token: String, client: String, uid: String) -> HTTPHeaders {
        let header: HTTPHeaders = ["Content-Type": "application/json", "access-token": token, "client": client, "uid": uid]
        return header
    }
    
    // MARK: - Função  publica videos
    func login(parms:[String: Any], onComplete: @escaping (User?, String) -> Void) {
        Alamofire.request(URL_SINGIN, method: .post, parameters: parms).validate(statusCode: 200..<300).responseJSON { (response) in
            let statusCode = response.response?.statusCode
            switch response.result {
            case .success:
                self.token = (response.response?.value(forHTTPHeaderField: "access-token"))!
                self.client = (response.response?.value(forHTTPHeaderField: "client"))!
                self.uid = (response.response?.value(forHTTPHeaderField: "uid"))!
                if let json = response.result.value as? NSDictionary {
                    print(json)
                    if let result:[String:Any] = json["investor"] as? [String:Any] {
                        let user = User(dictionary: result as Dictionary<String, AnyObject>)
                        print(user)
                        onComplete(user, "")
                    }
                }
            case .failure(let error):
                print("ERRO: \(error.localizedDescription)")
                switch statusCode {
                case 401:
                    onComplete(nil, "Usuario ou senha incorretos.")
                case 400:
                    onComplete(nil, "Falha na ao conectar ao servidor.")
                case 403:
                    onComplete(nil, "Usuario não tem permissão.")
                case 404:
                    onComplete(nil, "Servidor não encontrado.")
                case 408:
                    onComplete(nil, "Solicitação ultrapassou o tempo esperado.")
                case 500:
                    onComplete(nil, "Erro não identificado no servidor.")
                default:
                    onComplete(nil, "Erro desconhecido")
                }
            }
        }
    }
    
    func searchEmp(parm: String, onComplete: @escaping ([Empresa]?, String) -> Void) {
        let header = createHeaders(token: token, client: client, uid: uid)
        Alamofire.request("\(URL_EMPRESA)\(parm)", method: .get , parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            var result:[Empresa] = []
            let statusCode = response.response?.statusCode
            switch response.result {
            case .success:
                if let json = response.result.value as? NSDictionary {
                    if let array:[[String:Any]] = json["enterprises"] as? [[String:Any]] {
                        print(array)
                        for item in array {
                            let empresa = Empresa(dictionary: item as Dictionary<String, AnyObject>)
                            result.append(empresa)
                        }
                        onComplete(result, "")
                    }
                }
            case .failure(let error):
                print("ERRO: \(error.localizedDescription)")
                switch statusCode {
                case 401:
                    onComplete(nil, "Usuario não autenticado.")
                case 400:
                    onComplete(nil, "Falha na ao conectar ao servidor.")
                case 403:
                    onComplete(nil, "Usuario não tem permissão.")
                case 404:
                    onComplete(nil, "Servidor não encontrado.")
                case 408:
                    onComplete(nil, "Solicitação ultrapassou o tempo esperado.")
                case 500:
                    onComplete(nil, "Erro não identificado no servidor.")
                default:
                    onComplete(nil, "Erro desconhecido")
                }
            }
        }
    }
    
    
}
