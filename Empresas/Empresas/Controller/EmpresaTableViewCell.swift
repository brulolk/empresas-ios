//
//  EmpresaTableViewCell.swift
//  Empresas
//
//  Created by Bruno Vinicius on 19/12/19.
//  Copyright © 2019 Bruno Vinicius. All rights reserved.
//

import UIKit

class EmpresaTableViewCell: UITableViewCell {
    
    @IBOutlet weak var photoImg: UIImageView!
    @IBOutlet weak var empresaLabel: UILabel!
    @IBOutlet weak var typeLAbel: UILabel!
    @IBOutlet weak var paisLabel: UILabel!
    @IBOutlet weak var viewLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func prepare(emp: Empresa) {
        empresaLabel.text = emp.enterprise_name
        typeLAbel.text = emp.enterprise_type
        paisLabel.text = emp.country
        if emp.photo == ""{
            viewLabel.text = String(emp.enterprise_name.prefix(2)).uppercased()
        } else {
            viewLabel.isHidden = true
        }
    }

}
