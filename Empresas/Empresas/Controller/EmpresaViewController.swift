//
//  EmpresaViewController.swift
//  Empresas
//
//  Created by Bruno Vinicius on 20/12/19.
//  Copyright © 2019 Bruno Vinicius. All rights reserved.
//

import UIKit

class EmpresaViewController: UIViewController {
    
    @IBOutlet weak var empImage: UIImageView!
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var textLabel: UILabel!
    
    var emp = Empresa()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        overrideUserInterfaceStyle = .light
        
        navigationItem.title = emp.enterprise_name
        descriptionText.text = emp.description
        textLabel.text = String(emp.enterprise_name.prefix(2)).uppercased()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func searchAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
