//
//  HomeViewController.swift
//  Empresas
//
//  Created by Bruno Vinicius on 19/12/19.
//  Copyright © 2019 Bruno Vinicius. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var empresasTBV: UITableView!
    
    let image = UIImage(named: "logoNav")!
    var empresas: [Empresa] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        overrideUserInterfaceStyle = .light
        
        empresasTBV.delegate = self
        empresasTBV.dataSource = self
        setupUI()
        setupLogo()
        // Do any additional setup after loading the view.
    }
    
    func setupUI() {
        empresasTBV.isHidden = true
    }
    
    func setupLogo() {
        let imageSize = CGSize(width: 60, height: 42)
        let marginX: CGFloat = (self.navigationController!.navigationBar.frame.size.width / 2) - (imageSize.width / 2)
        let imageView = UIImageView(frame: CGRect(x: marginX, y: 0, width: imageSize.width, height: imageSize.height))
        imageView.image = image
        imageView.contentMode = .scaleAspectFit

        self.navigationItem.titleView = imageView
    }
    
    func searchEmpresas(text: String) {
        let parms = text.replacingOccurrences(of: " ", with: "+")
        DataService.shared.searchEmp(parm: parms) { (result, erro) in
            if result != nil {
                if result!.count >= 1 {
                    self.empresas = result!
                    self.empresasTBV.isHidden = false
                    self.empresasTBV.reloadData()
                } else {
                    self.empresasTBV.isHidden = true
                    self.infoLabel.text = "Nenhum empresa encontrada com esse nome."
                }
            } else {
                self.showErrorMessage("ERRO!", message: erro, duration: 5.0)
            }
        }
    }
    
    //MARK: Prepare
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "empSegue" {
            let vc = segue.destination as! EmpresaViewController
            vc.emp = empresas[empresasTBV.indexPathForSelectedRow!.row]
        }
    }
    
    @objc func dismissSearch() {
        empresasTBV.isHidden = true
        setupLogo()
        let cancelSearchBarButtonItem = UIBarButtonItem(image: UIImage(named: "icSearchCopy"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(searchAction(_:)))
        cancelSearchBarButtonItem.tintColor = UIColor.white
        let logoutBarButtonItem = UIBarButtonItem(title: "Logout", style: UIBarButtonItem.Style.plain, target: self, action: #selector(Logout(_:)))
        cancelSearchBarButtonItem.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = cancelSearchBarButtonItem
        self.navigationItem.leftBarButtonItem = logoutBarButtonItem
        self.infoLabel.text = "Volte a buscar."
    }
    
    @IBAction func searchAction(_ sender: Any) {
        let searchBar = UISearchBar()
        searchBar.showsCancelButton = false
        searchBar.tintColor = UIColor.white
        searchBar.placeholder = "Pesquisar"
        searchBar.delegate = self
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.backgroundColor = UIColor.white
        textFieldInsideSearchBar?.textColor = UIColor.black
        
        if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
            if let backgroundview = textfield.subviews.first {
                backgroundview.backgroundColor = UIColor.white
                backgroundview.layer.cornerRadius = 10;
                backgroundview.clipsToBounds = true;

            }
        }
        
        let cancelSearchBarButtonItem = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(dismissSearch))
        cancelSearchBarButtonItem.tintColor = UIColor.white
        
        self.navigationItem.titleView?.tintColor = UIColor.white
        self.navigationItem.titleView = searchBar
        self.navigationItem.rightBarButtonItem = cancelSearchBarButtonItem
        self.navigationItem.leftBarButtonItem = .none
    }
    
    @IBAction func Logout(_ sender: Any) {
        self.dismiss(animated: true) {
            DataService.shared.client = ""
            DataService.shared.token = ""
            DataService.shared.uid = ""
        }
    }
    
    

}

extension HomeViewController: UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return empresas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "empCell", for: indexPath) as! EmpresaTableViewCell
        let empresa = empresas[indexPath.row]
        cell.prepare(emp: empresa)
        return cell
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchEmpresas(text: searchText)
    }
    
}
