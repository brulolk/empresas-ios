//
//  LoginViewController.swift
//  Empresas
//
//  Created by Bruno Vinicius on 19/12/19.
//  Copyright © 2019 Bruno Vinicius. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var enterButton: UIButton!
    
    var params: [String : Any] = [:]
    var user = User()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        overrideUserInterfaceStyle = .light
        setupUI()
    }
    
    func setupUI() {
        setupTextField(textField: emailTxtField)
        setupTextField(textField: passwordTextField)
        
        let emailImg = UIImage(named: "icEmail")
        let leftEmailView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: emailImg!.size.width, height: emailImg!.size.height))
        leftEmailView.image = emailImg
        emailTxtField.leftView = leftEmailView
        emailTxtField.leftViewMode = .always
        
        let passwordImg = UIImage(named: "icCadeado")
        let leftPassView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: passwordImg!.size.width, height: passwordImg!.size.height))
        leftPassView.image = passwordImg
        passwordTextField.leftView = leftPassView
        passwordTextField.leftViewMode = .always
        
        
        enterButton.layer.cornerRadius = 6
        enterButton.clipsToBounds = true
    }
    
    func setupTextField(textField: UITextField) {
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0.0, y: textField.frame.height, width: textField.frame.width, height: 1.0)
        bottomLine.backgroundColor = UIColor.darkGray.cgColor
        textField.borderStyle = UITextField.BorderStyle.none
        textField.layer.addSublayer(bottomLine)
    }
    
    func login() {
        DataService.shared.login(parms: params) { (response, erro) in
            if response != nil {
                self.user = response!
                self.showSuccessMessage("Sucesso", message: "Seja bem vindo \(self.user.investor_name!)", duration: 5.0)
                self.presentHomeVC()
                self.hideProgress()
            } else {
                self.hideProgress()
                self.showErrorMessage("ERRO!", message: erro, duration: 5.0)
            }
        }
    }
    
    func validateText() -> Bool {
        if emailTxtField.text!.isEmpty {
            hideProgress()
            showAlert(title: "E-mail", message: "O campo obrigatorio!")
            return false
        }
        if !isValidEmail(emailStr: emailTxtField.text!) {
            hideProgress()
            showAlert(title: "E-mail", message: "Digite um E-mail valido!")
            return false
        }
        if passwordTextField.text!.isEmpty {
            hideProgress()
            showAlert(title: "Senha", message: "O campo obrigatorio!")
            return false
        }
        return true
    }
    
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    func setupParms() {
        params = ["email": emailTxtField.text!, "password": passwordTextField.text!]
    }
    
    func presentHomeVC() {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVC")
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func enterAction(_ sender: Any) {
        self.showProgress()
        if validateText() {
            setupParms()
            //presentHomeVC()
            login()
        }
    }
    

}
