//
//  User.swift
//  Empresas
//
//  Created by Bruno Vinicius on 19/12/19.
//  Copyright © 2019 Bruno Vinicius. All rights reserved.
//

import Foundation

class User{
    
    var id: Int!
    var investor_name: String!
    var email: String!
    var city: String!
    var country: String!
    
    init() {
        self.id = 0
        self.investor_name = ""
        self.email = ""
        self.city = ""
        self.country = ""
    }
    
    
    init(id: Int, investor_name: String, email: String, city: String, country: String) {
        self.id = id
        self.investor_name = investor_name
        self.email = email
        self.city = city
        self.country = country
    }
    
    
    convenience init(dictionary: Dictionary<String, AnyObject>) {
        //print("dicionario = \(dictionary)")
        let id = dictionary["id"] as? Int
        let investor_name = dictionary["investor_name"] as? String
        let email = dictionary["email"] as? String
        let city = dictionary["city"] as? String
        let country = dictionary["country"] as? String
        
        self.init(id: id!, investor_name: investor_name!, email: email!, city: city!, country: country!)
    }
}
