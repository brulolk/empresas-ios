//
//  Empresa.swift
//  Empresas
//
//  Created by Bruno Vinicius on 19/12/19.
//  Copyright © 2019 Bruno Vinicius. All rights reserved.
//

import Foundation

class Empresa{
    
    var id: Int!
    var enterprise_name: String!
    var description: String!
    var photo: String?
    var city: String!
    var country: String!
    var enterprise_type: String!
    
    init() {
        self.id = 0
        self.enterprise_name = ""
        self.description = ""
        self.photo = nil
        self.city = ""
        self.country = ""
        self.enterprise_type = ""
    }
    
    
    init(id: Int, enterprise_name: String, description: String, photo: String?, city: String, country: String, enterprise_type: String) {
        
        self.id = id
        self.enterprise_name = enterprise_name
        self.description = description
        self.photo = photo
        self.city = city
        self.country = country
        self.enterprise_type = enterprise_type
    }
    
    
    convenience init(dictionary: Dictionary<String, AnyObject>) {
        //print("dicionario = \(dictionary)")
        let id = dictionary["id"] as? Int
        let enterprise_name = dictionary["enterprise_name"] as? String
        var description = dictionary["description"] as? String
        if description == nil {
            description = "Sem descricão"
        }
        var photo = dictionary["photo"] as? String
        if photo == nil {
            photo = ""
        }
        let city = dictionary["city"] as? String
        let country = dictionary["country"] as? String
        let enterprise_type = (dictionary["enterprise_type"] as! [String: Any])["enterprise_type_name"] as? String
        
        self.init(id: id!, enterprise_name: enterprise_name!, description: description!, photo: photo, city: city!, country: country!, enterprise_type: enterprise_type!)
    }
    
}

